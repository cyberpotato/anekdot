const express = require("express");
var http = require("http")
var iconv = require("iconv-lite");

const tigr = `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1"><title>
	Случайный анекдот-3
</title></head>
<body style="
	font-family:Arial, Helvetica, sans-serif;
	margin: 0;
	padding: 0;
    width:100%;
	font-size:13px;
	background-color:Transparent;	
	color:Black; background-image: none;">
	<div style="
	top:0;
	left:0;">
    <form name="form1" method="post" action="./Anekdot2.aspx" id="form1">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTEyMTA4NTIyOTMPZBYCAgMPZBYEAgEPDxYCHgRUZXh0BbsC0JzQvtC70L7QtNCw0Y8g0LbQtdC90YnQuNC90LAg0LLRj9C20LXRgiDQvNCw0LvQtdC90YzQutC40LUg0YfRg9C70L7Rh9C60LguPGJyIC8+LSDQlNC+0YDQvtCz0L7QuSwgLSDQvdC10LbQvdC+INCz0L7QstC+0YDQuNGCINC+0L3QsCDQvNGD0LbRgywgLSDRgtGLINGF0L7RgtC10Lsg0LHRiyDRg9GB0LvRi9GI0LDRgtGMLCDQutCw0Log0L/QviDQvdCw0YjQtdC5INC60LLQsNGA0YLQuNGA0LUg0LHQtdCz0LDRjtGCINC80LDQu9C10L3RjNC60LjQtSDQvdC+0LbQutC4PzxiciAvPi0g0J3QtdGCLCDRjyDQvdC1INC70Y7QsdC70Y4g0LzRi9GI0LXQuS4gZGQCAw8PFgIfAAUFMTcwNTRkZGS30KBwGLCON2GsfDHVhqtL" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="EA6F5D54" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAAJ6oHUQEzjYWn/sezC9FGgstTfNpOz0CH4vKngBfzxDIS8fGxLRcmcbPXHzg+j9zw4=" />
    <div style="text-align:left">
    <span style="white-space: pre-line" id="Label1">- Как поймать тигра в клетку?\n- Никак! Тигры бывают только в полоску :)</span>
    <br />
    </div>
    <div style=" text-align:center">
    <a id="LinkButton1" href="javascript:alert('этот анекдот должны знать все!')">еще...</a>
    <br />
    </div>    
    </form>
    </div>
</body>
</html>`;

const getAnek = (url) => {
  const r = Math.random() <= 0.1;
  if (r) return tigr;
  return new Promise((resolve, reject) => {
    http.get(url, function (res) {
      res
        .pipe(iconv.decodeStream("win1251"))
        .collect(function (err, decodedBody) {
          if (err) {
            reject(err);
          } else {
            resolve(decodedBody.replace(/javascript:__doPostBack\(&#39;LinkButton1&#39;,&#39;&#39;\)/g, "javascript:document.location.href = document.location.href;"));
          }
        });
    });
  });
};

export default async function anek(request, response) {
    const anek = await getAnek("http://rzhunemogu.ru/Widzh/Anekdot2.aspx");
  response.status(200).send(anek);
}
